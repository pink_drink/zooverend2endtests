import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;

public class Common {

    public static String getRandomString(int lenght){

        return RandomStringUtils.randomAlphabetic(lenght);
    }

    public static int getRandomInteger(int count){
        Random rand = new Random();
        return rand.nextInt(9);
    }


    public static boolean IsElementExists(WebElement source, By by){
        WebElement element = source.findElement(by);
        if (element!=null) return true;
        else return false;
    }

    public static boolean IsElementExists(By by, WebDriver driver){
        WebElement element = driver.findElement(by);
        if (element!=null) return true;
        else return false;
    }

}
