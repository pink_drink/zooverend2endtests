import Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class HomePageTests extends BaseTest {
    public HomePageTests() throws Exception {
    }


    /**
     * go to main page
     * click review button
     * validate current url changed to search page
     * @throws Exception
     */
    @Test
    public void VerifyAddReviewButtonRefersToSearchPageUrl() throws Exception {

        for (WebDriver driver :drivers)
        {
            HomePage homePage = new HomePage(driver);
            driver.get(baseUrl);

            //when
            homePage.addReview.click();

            //then
            Assert.assertEquals(searchUrl, driver.getCurrentUrl());
        }
    }
}
