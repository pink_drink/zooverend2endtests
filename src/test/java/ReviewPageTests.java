import Pages.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.List;

public class ReviewPageTests extends BaseTest{
    public ReviewPageTests() throws Exception {
    }

    private static final String HOTEL_NAME = "Royal Alhambra Palace";
    private static final String QA_NAME_PREFIX = "[TEST]";
    private static final String OPERATOR_EXPEDIA = "Expedia";


    /**
     * fill review form of Review page with valid data and submit
     * choose Expedia operator on Operators page and set rating point and submit
     * validate form was submitted and Bedankt text is shown on a Bedankt page
     * @throws Exception
     */
    @Test
    public void FillReviewFormWithAllValidData() throws Exception {

        for (WebDriver driver :drivers) {

            SearchPage searchPage = new SearchPage(driver);

            driver.get(searchUrl);
            searchPage.searchField.click();
            searchPage.searchField.sendKeys(HOTEL_NAME);
            searchPage.searchButton.click();
            searchPage.searchField.click();
            searchPage.waitForElementToAppear(By.xpath(searchPage.HOTEL_ITEM_XPATH));

            // get item with Hotel name
            searchPage.hotelsList.stream().filter(hotel -> Common.IsElementExists(hotel, By.xpath("//div[contains(text(),'" + HOTEL_NAME + "')]")))
                    .findFirst().get().click();

            ReviewPage reviewPage = new ReviewPage(driver);

            //when
            searchPage.waitForElementToAppear(By.xpath(reviewPage.CREATE_REVIEW_TEXT_XPATH));

            //then
            Assert.assertTrue(reviewPage.writeReviewText.getText().equals("Schrijf een review"));

            reviewPage.reviewerName.sendKeys(QA_NAME_PREFIX + Common.getRandomString(5));

            reviewPage.travelAsFriends.click();

            reviewPage.year2017.click();

            reviewPage.mei.click();

            reviewPage.descriptionForHotel.sendKeys(
                    Common.getRandomString(10) + " " +
                            Common.getRandomString(10) + " " +
                            Common.getRandomString(10) + " " +
                            Common.getRandomString(10) + " " +
                            Common.getRandomString(10));

            reviewPage.sammariseForHotel.sendKeys(Common.getRandomString(15));

            fillHotelRatingsRandom(reviewPage, 9);

            reviewPage.plusRatingButton.click();
            reviewPage.emailField.sendKeys(Common.getRandomString(15)+"@gmail.com");

            JavascriptExecutor jse = (JavascriptExecutor)driver;

            jse.executeScript("document.getElementById('terms').click();");

            jse.executeScript("document.getElementById('newsletter').click();");

            reviewPage.submitButton.click();

            OperatorsPage operatorsPage = new OperatorsPage(driver);

            operatorsPage.waitForElementToAppear(By.cssSelector(operatorsPage.THANK_HEADER_CSS));

            operatorsPage.loadMoreOperatorsButton.click();

            operatorsPage.operatorsList.stream().filter(o-> o.getText().equals(OPERATOR_EXPEDIA)).findFirst().get().click();

            operatorsPage.operatorsRaitingPoint.get(9).click();

            operatorsPage.sendFeedbackButton.click();

            BedanktPage bedanktPage = new BedanktPage(driver);
            //then
            Assert.assertTrue(bedanktPage.thankHeader.getText().equals("Bedankt!"));

        }
    }

    /**
     * fill review form with empty reviewer name
     * submit
     * validate form is not submitted and hint to "fill user name is shown"
     * @throws Exception
     */
    @Test
    public void ShouldNotBePossibleToSendReviewWithEmptyReviewerName() throws Exception {

        for (WebDriver driver : drivers) {

            SearchPage searchPage = new SearchPage(driver);
            driver.get(searchUrl);
            searchPage.searchField.click();
            searchPage.searchField.sendKeys(HOTEL_NAME);
            searchPage.searchButton.click();
            searchPage.searchField.click();

            //then
            searchPage.waitForElementToAppear(By.xpath(searchPage.HOTEL_ITEM_XPATH));

            // get item with Hotel name
            searchPage.hotelsList.stream().filter(hotel -> Common.IsElementExists(hotel, By.xpath("//div[contains(text(),'" + HOTEL_NAME + "')]")))
                    .findFirst().get().click();

            ReviewPage reviewPage = new ReviewPage(driver);
            //when
            searchPage.waitForElementToAppear(By.xpath(reviewPage.CREATE_REVIEW_TEXT_XPATH));

            //then
            Assert.assertTrue(reviewPage.writeReviewText.getText().equals("Schrijf een review"));

            reviewPage.reviewerName.sendKeys("");

            reviewPage.travelAsFriends.click();

            reviewPage.year2017.click();

            reviewPage.mei.click();

            reviewPage.descriptionForHotel.sendKeys("");

            reviewPage.sammariseForHotel.sendKeys(Common.getRandomString(15));

            fillHotelRatingsRandom(reviewPage, 9);

            reviewPage.plusRatingButton.click();

            reviewPage.emailField.sendKeys(Common.getRandomString(15) + "@gmail.com");

            JavascriptExecutor jse = (JavascriptExecutor) driver;

            jse.executeScript("document.getElementById('terms').click();");

            jse.executeScript("document.getElementById('newsletter').click();");

            reviewPage.submitButton.click();
            // then
            Assert.assertTrue(Common.IsElementExists(By.xpath(reviewPage.ERROR_EMPTY_NAME_XPATH), driver));

        }
    }

    /**
     * fill review form with empty Description
     * submit
     * validate form is not submitted and submit button still shown
     * @throws Exception
     */
    @Test
    public void ShouldNotBePossibleToSendReviewWithEmptyDescription() throws Exception {

        for (WebDriver driver : drivers) {

            SearchPage searchPage = new SearchPage(driver);
            driver.get(searchUrl);
            searchPage.searchField.click();
            searchPage.searchField.sendKeys(HOTEL_NAME);
            searchPage.searchButton.click();
            searchPage.searchField.click();

            //then
            searchPage.waitForElementToAppear(By.xpath(searchPage.HOTEL_ITEM_XPATH));

            // get item with Hotel name
            searchPage.hotelsList.stream().filter(hotel -> Common.IsElementExists(hotel, By.xpath("//div[contains(text(),'" + HOTEL_NAME + "')]")))
                    .findFirst().get().click();

            ReviewPage reviewPage = new ReviewPage(driver);
            //when
            searchPage.waitForElementToAppear(By.xpath(reviewPage.CREATE_REVIEW_TEXT_XPATH));

            //then
            Assert.assertTrue(reviewPage.writeReviewText.getText().equals("Schrijf een review"));

            reviewPage.reviewerName.sendKeys(QA_NAME_PREFIX + Common.getRandomString(5));

            reviewPage.travelAsFriends.click();

            reviewPage.year2017.click();

            reviewPage.mei.click();

            reviewPage.sammariseForHotel.sendKeys(Common.getRandomString(15));

            fillHotelRatingsRandom(reviewPage, 9);

            reviewPage.plusRatingButton.click();

            reviewPage.emailField.sendKeys(Common.getRandomString(15) + "@gmail.com");

            JavascriptExecutor jse = (JavascriptExecutor) driver;

            jse.executeScript("document.getElementById('terms').click();");

            jse.executeScript("document.getElementById('newsletter').click();");

            reviewPage.submitButton.click();
            // then
            Assert.assertTrue(Common.IsElementExists(By.cssSelector(reviewPage.SUBMIT_BUTTON_CSS), driver));

        }
    }

    private void fillHotelRatingsRandom(ReviewPage reviewPage, int randomMaxRate) {

        reviewPage.liggingRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.serviceRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.prijsKwaliteitRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.etenRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.kamersRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.kindvriendelijkRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.zwembadRange.get(Common.getRandomInteger(randomMaxRate)).click();

        reviewPage.hygiëneRange.get(Common.getRandomInteger(randomMaxRate)).click();
    }

}
