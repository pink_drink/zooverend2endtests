package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ReviewPage extends BasePage {
    public ReviewPage(WebDriver driver) throws Exception {
        super(driver);

    }

    public final String CREATE_REVIEW_TEXT_XPATH = "//span[contains(text(),'Schrijf een review')]";
    public final String REVIEWER_NAME_CSS = "input[name='name']";
    public final String TRAVEL_AS_FRIENDS_CSS = "div[label='Met vrienden']";
    public final String TRAVEL_AS_COUPLE_CSS = "div[label='Met partner']";
    public final String TRAVEL_AS_PERSON_CSS = "div[label='Alleen']";
    public final String YEAR_2019_CSS = "div[label='2019']";
    public final String YEAR_2018_CSS = "div[label='2018']";
    public final String YEAR_2017_CSS = "div[label='2017']";
    public final String MEI = "div[label='mei']";
    public final String DESCRIPTION_CSS = "textarea[name='text']";
    public final String SAMMARISE_CSS = "input[name='title']";
    public final String LIGGING_CSS = "div[label='Ligging'] div[label]";
    public final String SERVICE_CSS = "div[label='Service'] div[label]";
    public final String PRIJS_CSS = "div[label='Prijs / kwaliteit'] div[label]";
    public final String ETEN_CSS = "div[label='Eten'] div[label]";
    public final String KAMERS_CSS = "div[label='Kamers'] div[label]";
    public final String KINDVRIENDELIJK_CSS = "div[label='Kindvriendelijk'] div[label]";
    public final String ZWEMBAD_CSS = "div[label='Zwembad'] div[label]";
    public final String HYGI_CSS = "div[label='Hygiëne'] div[label]";
    public final String PLUS_BUTTON_CSS = "div .rightSign";
    public final String EMAIL_INPUT_CSS = "input[type='email']";
    public final String SUBMIT_BUTTON_CSS = ".qaSendReviewButton";
    public final String ERROR_EMPTY_NAME_XPATH = "//span[contains(text(),'Vul jouw naam in')]";
    public final String ERROR_EMPTY_DESCRIPTION_XPATH = "//span[contains(text(),'Het geven van een toelichting is verplicht.')]";


    @FindBy(xpath=CREATE_REVIEW_TEXT_XPATH)
    public WebElement writeReviewText;

    @FindBy(css=REVIEWER_NAME_CSS)
    public WebElement reviewerName;

    @FindBy(css=TRAVEL_AS_PERSON_CSS)
    public WebElement travelAsPerson;

    @FindBy(css=TRAVEL_AS_COUPLE_CSS)
    public WebElement travelAsCouple;

    @FindBy(css=TRAVEL_AS_FRIENDS_CSS)
    public WebElement travelAsFriends;

    @FindBy(css=YEAR_2019_CSS)
    public WebElement year2019;

    @FindBy(css=YEAR_2018_CSS)
    public WebElement year2018;

    @FindBy(css=YEAR_2017_CSS)
    public WebElement year2017;

    @FindBy(css=MEI)
    public WebElement mei;

    @FindBy(css=DESCRIPTION_CSS)
    public WebElement descriptionForHotel;

    @FindBy(css=SAMMARISE_CSS)
    public WebElement sammariseForHotel;

    @FindAll(@FindBy(css=LIGGING_CSS))
    public List<WebElement> liggingRange;

    @FindAll(@FindBy(css=SERVICE_CSS))
    public List<WebElement> serviceRange;

    @FindAll(@FindBy(css=PRIJS_CSS))
    public List<WebElement> prijsKwaliteitRange;

    @FindAll(@FindBy(css=ETEN_CSS))
    public List<WebElement> etenRange;

    @FindAll(@FindBy(css=KAMERS_CSS))
    public List<WebElement> kamersRange;

    @FindAll(@FindBy(css=KINDVRIENDELIJK_CSS))
    public List<WebElement> kindvriendelijkRange;

    @FindAll(@FindBy(css=ZWEMBAD_CSS))
    public List<WebElement> zwembadRange;

    @FindAll(@FindBy(css=HYGI_CSS))
    public List<WebElement> hygiëneRange;

    @FindBy(css=PLUS_BUTTON_CSS)
    public WebElement plusRatingButton;

    @FindBy(css=EMAIL_INPUT_CSS)
    public WebElement emailField;

    @FindBy(css=SUBMIT_BUTTON_CSS)
    public WebElement submitButton;

    @FindBy(xpath=ERROR_EMPTY_NAME_XPATH)
    public WebElement errorEmptyName;


}
