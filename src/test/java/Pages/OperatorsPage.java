package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class OperatorsPage extends BasePage {

    public OperatorsPage(WebDriver driver) throws Exception {
        super(driver);
    }

    public final String MORE_OPERATORS_BUTTON_CSS = "button[data-qa='toggle-button']";
    public final String THANK_HEADER_CSS = ".qaThankHeader";
    public final String OPERATOR_CSS = "button";
    public final String OPERATOR_POINI_CSS = ".qaPlate";
    public final String SEND_FEEDBACK_HPATH = "div[data-qa='button-']";


    @FindBy(css=MORE_OPERATORS_BUTTON_CSS)
    public WebElement loadMoreOperatorsButton;

    @FindBy(css=THANK_HEADER_CSS)
    public WebElement thankHeader;

    @FindAll(@FindBy(css=OPERATOR_CSS))
    public List<WebElement> operatorsList;

    @FindAll(@FindBy(css=OPERATOR_POINI_CSS))
    public List<WebElement> operatorsRaitingPoint;

    @FindBy(css=SEND_FEEDBACK_HPATH)
    public WebElement sendFeedbackButton;
}
