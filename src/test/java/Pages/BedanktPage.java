package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BedanktPage extends BasePage {

    public BedanktPage(WebDriver driver) throws Exception {
        super(driver);
    }

    public final String THANK_HEADER_CSS = ".qaThankHeader";

    @FindBy(css=THANK_HEADER_CSS)
    public WebElement thankHeader;
}
