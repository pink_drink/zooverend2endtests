package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class SearchPage extends BasePage {

    private static final String SEARCH_FIELD_CSS = "input[type='search']";
    private static final String SEARCH_BUTTON_CSS = "button[type='submit']";
    public static final String HOTEL_ITEM_XPATH = "//a[starts-with(@data-qa,'picker-suggestion-result')]";

    public SearchPage(WebDriver driver) throws Exception {
        super(driver);
    }

    @FindBy(css=SEARCH_FIELD_CSS)
    public WebElement searchField;

    @FindAll(@FindBy(xpath=HOTEL_ITEM_XPATH))
    public List<WebElement> hotelsList;

    @FindBy(css=SEARCH_BUTTON_CSS)
    public WebElement searchButton;

}
