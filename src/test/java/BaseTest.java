import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Properties;

public class BaseTest {

    private Properties prop;
    private static final String TESTING_PROPERTIES_LOCATION = "src/test/resources/config.properties";
    private String ENVIRONMENT_NAME = "environment.name";
    private String SEARCH_PAGE = "search.page";
    private String BROWSERS = "browsers";
    private String driversList;
    ArrayList<WebDriver> drivers;

    String baseUrl;
    String searchUrl;


    public BaseTest() throws Exception {
        prop = new Properties();
        FileInputStream fileInput = new FileInputStream(TESTING_PROPERTIES_LOCATION);
        prop.load(fileInput);
        driversList = prop.getProperty(BROWSERS);
        baseUrl = prop.getProperty(ENVIRONMENT_NAME);
        searchUrl = baseUrl + prop.getProperty(SEARCH_PAGE);
    }

    @BeforeMethod
    public void setUp(){
        drivers = getBrowsersList();
        for (WebDriver driver: drivers)
            driver.manage().window().maximize();
    }

    protected ArrayList<WebDriver> getBrowsersList(){

        ArrayList<WebDriver> drivers = new ArrayList<WebDriver>();
        for (String browser : driversList.split("\\|")) {

            if (browser.equals("chrome")){
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
                drivers.add(new ChromeDriver());
            }

            if (browser.equals("gecko")){
                System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
                drivers.add(new FirefoxDriver());
            }
        }
        return drivers;
    }

    @AfterMethod
    public void afterSuite(){

        if (null != drivers) {
            for (WebDriver driver : drivers) {
                driver.quit();
            }
        }
    }

}
