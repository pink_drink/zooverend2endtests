# ZOOVER tests end to end #

Hello, this is a readme file for test project for zoover

### Technical information ###

* Mac OS
* JAVA version 8, Maven
* Selenium, Firefox and Chrome drivers, TestNg
* Page Object framework for selenium

### Configure browsers ###

in a config file there is a property browsers=chrome|gecko which is used to start  Chrome and Firefox
if any problems with Firefox you can switch it off with set "browsers=chrome" then only Chrome will be used


### How to run and build ###

* please use Mac OS for building
* After project is cloned from repo, you need to open command line and
go inside project folder "zooverend2endtests" where pom.xml file is located
* Run "mvn clean install" command inside it 
* Now when BUILD SUCCESS we can run tests


### RUN tests ###
The main test with main script for the task is "FillReviewFormWithAllValidData" in a class ReviewPageTests
* you can run it from IDE or from command line:
"mvn -Dtest=className test"

### Trouble shooting ###
sometime I faced problems with loading search result for Hotel in Firefox
when load scripts for some reason do not load hotels(it needs investigation)

if you will face this issue, rerun the test (it should succeed)

Happy testing!




